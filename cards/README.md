## 1.07b - Cards

**Aufgabe:** Schreiben Sie ein Programm, das ein Kartenspiel mit 52 Karten (mit 4 Farben: Clubs, Hearts, Diamonds, Spades) zu je 13 Karten (Ass, König, Dame, Bube, 10, 9, 8, 7, 6, 5, 4, 3, 2) auf eine über die Kommandozeile definierte Anzahl von Spielern verteilt.

## Anforderungen

- **Eingabe:** 
  - Das erste Kommandozeilenargument gibt die Anzahl der Spieler an.
  - Das zweite Kommandozeilenargument gibt an, wie viele Karten jeder Spieler erhalten soll.

Beispiel: `gradle run --args="2 3"` für 2 Spieler mit jeweils 3 Karten.

## Ausgabe
Das Programm soll folgende Teile ausgeben:

1. **Ausgabe des kompletten Kartenstapels zu Beginn.**
2. **Ausgabe der Teil-Kartensätze (der Hände) eines jeden Spielers.**
3. **Ausgabe des restlichen Kartenstapels.**

## Fehlerbehandlung
Falls der Kartenstapel die Anfrage nicht erfüllen kann, soll eine `java.lang.IllegalStateException` geworfen werden. Diese Exception muss eine aussagekräftige Nachricht enthalten.

- **Beispiel:** 
  - Es können keine 9 Karten an 6 Personen verteilt werden (54 Karten), da der Kartenstapel nur aus 52 Karten besteht.

## Hinweise
- **Repräsentation von Karten:** 
  - Karten können als `String`-Objekte repräsentiert werden, z. B. "Karo 6", "Herz Dame".
  
- **Kartenstapel:** 
  - Eine `Liste` eignet sich zur Darstellung des Kartenstapels.
  - Verwenden Sie die `shuffle`-Methode aus der `Collections`-Klasse, um den Kartenstapel zu mischen.
  - Mit der Methode `sublist(...)` kann man ein Blatt je Spieler definieren und aus dem restlichen Kartenstapel entfernen.
