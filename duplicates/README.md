## 1.07a - Duplicates

**Voraussetzung**: Vorlesung zum Collection Framework (bis einschließlich List und Set).

**Ziel**: Kennenlernen der API von List und Set anhand einfacher Beispiele.

**Dauer**: < 1h

Die Klassen des Java Collection Framework bieten eine Vielzahl an nützlichen Funktionalitäten. Die folgenden kleinen Übungsaufgaben sollen Sie mit dem Framework vertraut machen.

(a) Schreiben Sie eine ausführbare Klasse App, die die übergebenen Kommandozeilenargumente (String args[]) der main-Methode daraufhin überprüft, ob sie mehrfach oder einfach vorkommen. Dabei soll das Programm zwei Mengen (Set) bestimmen, wobei die eine der Menge der Duplikate und die andere der Menge der Unikate entspricht. Die Klasse soll am Ende die Menge der Duplikate und die Menge der Unikate ausgeben.

Ein Beispielaufruf kann folgenderweise aussehen:
```
gradle run --args"i came i saw i left"
```

Die Ausgabe hiervon wäre:
```
unique words: [cam, saw, left]
duplicate words: [i]
