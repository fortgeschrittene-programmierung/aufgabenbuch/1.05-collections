## 1.07c - Orders

Gegeben ist die Klasse `Order`.

Für die Verarbeitung in einer Prioritätswarteschlange (`PriorityQueue`) soll die natürliche Ordnung so gewählt werden, dass das `Order`-Objekt von VIP-Kunden immer höchste Priorität hat. Implementieren Sie die Schnittstelle `java.lang.Comparable` entsprechend und schreiben Sie in einer `Main`-Klasse einige Tests zum Überprfüfen der korrekten Reihenfolge.

Gehen Sie bei der Implementierung nach dem Prinzip des Test-Driven-Development (TDD) vor:
1. **Test Implementieren:** Schreiben Sie zuerst einen Test (`Main`-Klasse), der das gewünschte Verhalten der Prioritätswarteschlange überprüft. Der Test sollte zunächst scheitern, da die Klasse `Order` noch nicht angepasst wurde.

2. **Änderungen Vornehmen:** Programmieren Sie dann die notwendigen Änderungen in der Klasse `Order`, so dass der Test erfolgreich ist.
